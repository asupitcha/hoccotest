import React from 'react';
// import logo from './logo.svg';
import Header from './Component/Header'
import Content from './Component/Content'

function App() {
  return (
    <React.Fragment>
      <Header />
      <Content />
    </React.Fragment>
  );
}

export default App;
