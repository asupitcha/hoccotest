import React from 'react'

// function HelloApp(props) {
//     return <h1>Yooo! {props.name}</h1>
// }
class HelloApp extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            counter: 0
        }

        this.setState(prevState => ({
            counter: prevState.counter + 1
        }))
	}
    

    render() {
        return (
            <React.Fragment>
                <h1>Yo! React</h1>
                <p>{this.props.message}</p>
                <button onClick={this.handleClick}>Click me!</button>
                <p>Count : {this.state.counter}</p>
            </React.Fragment>
        )
    }
}

export default HelloApp