import React from 'react'
import './css/style.css'

class MenuBar extends React.Component {
    render() {
        return (
            <div class="Rectangle-490">
                <a class="ABOUT">Home</a>
                <a href="#news">News</a>
                <a href="#contact">Contact</a>
                <a href="#about">About</a>
            </div>
        )
    }
}

export default MenuBar