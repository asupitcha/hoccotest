import React from 'react'
import '../css/style.css'

class ContentHeader extends React.Component {
    render() {
        return (
            <React.Fragment>
                <div className="Rectangle-675">
                    <a href="" className="BLOG">BLOG</a>  
                    <div className="rightBar">
                        <a className="path">You are here :</a>
                        <a className="path">HOME > </a>
                        <a className="path">BLOG > </a>
                        <a className="path">Proin ac quam et …</a>
                    </div>
                </div>
                <div className="Mask-Group-47">
                    <img src={require('../css/bg.jpg')} style={{width:"100%", height:"100%"}}></img>    
                </div>
            </React.Fragment>
        )
    }
}

export default ContentHeader