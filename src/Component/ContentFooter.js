import React from 'react'
import '../css/style.css'

class ContentFooter extends React.Component {
    render() {
        return (
            <div className="Rectangle-597">
                <div className="footcontent">
                    <div className="Rectangle-752"></div>
                    <p className="contentfooter1">one-stop supply chain management solution for e-commerce and omni-channel businesses. Our order fulfillment service provides storing, packing, shipping service for your shops. 
                    </p>
                    <p className="contentfooter1">
                        Our professionals can help you manage your multi sales channel with multi procedures with ease.
                    </p>
                </div> 
            </div>
        )
    }
}

export default ContentFooter