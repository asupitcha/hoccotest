import React from 'react'
import '../css/style.css'

class ContentBody extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            market: "MARKETING",
            author: "BY MARKETING TEAM",
            date: " - 3 SEP 2019",
            title: "Proin ac quam et lectus vestibulum blandit. Nunc maximus nibh at placerat",
            content: "Duis mauris augue, efficitur eu arcu sit amet, posuere dignissim neque. Aenean enim sem, pharetra et magna sit amet, luctus aliquet nibh."
        }
    }

    render() {
        return (
            <div className="Rectangle-685">
                <div className="content">
                    <div className="Rectangle-710">
                        <p className="MARKETING">{this.state.market}</p>                        
                    </div>
                    <div className="author">
                        <a className="BY-MARKETING-TEAM">{this.state.author}</a>
                        <a className="date">{this.state.date}</a>          
                    </div>
                    <h1 className="title">
                        Donec dapibus mauris id odio ornare tempus. Duis sit amet accumsan justo, quis tempor ligula. 
                    </h1>
                    <img className="Mask-Group-50" src={require('../css/bg.jpg')}></img>
                    <blockquote class="otro-blockquote"> Praesent eu dolor eu orci vehicula euismod. Vivamus sed sollicitudin libero, 
                        vel malesuada velit. Nullam et maximus lorem. Suspendisse maximus dolor quis consequat volutpat. 
                        Donec vehicula elit eu erat pulvinar, vel congue ex egestas. Praesent egestas purus dolor, 
                        a porta arcu pharetra quis.
                    </blockquote>  
                    <p className="Velit-neque-posuere">
                        Velit neque posuere 
                    </p>   
                    <p className="h3.BLOG">
                        h1asdfasljdfa;lsdjf;asldfj h3.BLOG h3.BLOG h3.BLOG h3.BLOG
                    </p> 
                    <p className="Curabitur">
                        Curabitur vulputate arcu odio, ac facilisis diam accumsan ut. Ut imperdiet et leo in vulputate. Sed eleifend 
                        lacus eu sapien sagittis imperdiet. Etiam tempor mollis augue, ut tincidunt ex interdum eu. 
                        Pellentesque rhoncus lectus sed posuere viverra. Vestibulum id turpis lectus. Donec rhoncus quis 
                        elit sed fermentum. Nullam sit amet ex enim. Fusce nec suscipit nulla. Maecenas porta mi vestibulum, 
                        lobortis est ac, hendrerit dui. Pellentesque auctor id enim sit amet molestie.
                    </p>
                    <p className="Nunc">
                        Nunc scelerisque tincidunt elit. Vestibulum non mi ipsum. Cras pretium suscipit tellus sit amet aliquet. Vestibulum maximus lacinia massa non porttitor. Pellentesque vehicula est a lorem gravida bibendum. Proin tristique diam ut urna pharetra, ac rhoncus elit elementum. Proin vitae purus ultrices, dignissim turpis ut, mattis eros. Maecenas ornare molestie urna, hendrerit venenatis sem.
                    </p> 
                    <p className="Nunc">
                        Duis mauris augue, efficitur eu arcu sit amet, posuere dignissim neque. Aenean enim sem, pharetra et magna sit amet, luctus aliquet nibh. Curabitur auctor leo et libero consectetur gravida. Morbi gravida et sem dictum varius. Proin eget viverra sem, non euismod est. Maecenas facilisis urna in lectus aliquet venenatis. Etiam et metus nec mauris condimentum vulputate. Aenean volutpat odio quis egestas tempus. Fusce tempor vulputate luctus. Pellentesque vulputate viverra ex eget elementum. Aliquam ut feugiat felis.                    
                    </p> 
                    <p className="Nunc">
                        Curabitur vulputate arcu odio, ac facilisis diam accumsan ut. Ut imperdiet et leo in vulputate. Sed eleifend lacus eu sapien sagittis imperdiet. Etiam tempor mollis augue, ut tincidunt ex interdum eu. Pellentesque rhoncus lectus sed posuere viverra. Vestibulum id turpis lectus. Donec rhoncus quis elit sed fermentum. Nullam sit amet ex enim. Fusce nec suscipit nulla. Maecenas porta mi vestibulum, lobortis est ac, hendrerit dui. Pellentesque auctor id enim sit amet molestie.                    
                    </p>
                </div>
                <div>
                    <div className="foottext">
                        <h1 className="related">
                            บทความที่เกี่ยวข้อง 
                        </h1>
                        <div className="Rectangle-699"></div>
                    </div>
                    <div>
                    <div class="column">
                        <div className="Rectangle-741"></div>
                        <div className="Rectangle-710co">
                            <p className="MARKETINGcolumn">{this.state.market}</p>                        
                        </div>
                        <h2 className="titlecolumn">{this.state.title}</h2>
                        <div >
                            <a className="bycolumn">{this.state.author}</a>
                            <a className="datecolumn">{this.state.date}</a>          
                        </div>
                        <p>{this.state.content}</p>
                    </div>
                    <div class="column">
                        <div className="Rectangle-741"></div>
                        <div className="Rectangle-710co">
                            <p className="MARKETINGcolumn">{this.state.market}</p>                        
                        </div>
                        <h2 className="titlecolumn">{this.state.title}</h2>
                        <div >
                            <a className="bycolumn">{this.state.author}</a>
                            <a className="datecolumn">{this.state.date}</a>          
                        </div>
                        <p>{this.state.content}</p>
                    </div>
                    <div class="column">
                        <div className="Rectangle-741"></div>
                        <div className="Rectangle-710co">
                            <p className="MARKETINGcolumn">{this.state.market}</p>                        
                        </div>
                        <h2 className="titlecolumn">{this.state.title}</h2>
                        <div >
                            <a className="bycolumn">{this.state.author}</a>
                            <a className="datecolumn">{this.state.date}</a>          
                        </div>
                        <p>{this.state.content}</p>
                    </div>
                </div>        
            </div>
        </div>
        )
    }
}

export default ContentBody