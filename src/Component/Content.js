import React from 'react'
import '../css/style.css'
import ContentHeader from './ContentHeader'
import ContentBody from './ContentBody'
import Footer from './Footer'
import ContentFooter from './ContentFooter'

class Content extends React.Component {
    render() {
        return (
            <React.Fragment>
                <ContentHeader />
                <ContentBody />
                <ContentFooter />
                <Footer />
            </React.Fragment>
        )
    }
}

export default Content